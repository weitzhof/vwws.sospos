# stdlib
import logging

# 3rd party
from webob.exc import HTTPForbidden

# projekt
from vwws.common import view
from vwws.common.model import AppRoot
from vwws.common.util import SmartJSONEncoder, time_this
from vwws.sospos import app
from vwws.sospos import model
from vwws.sospos.permission import ReadPermission


log = logging.getLogger(__name__)


# TODO: für Produktivbetrieb ist dies VIEL zu langsam!
# render_json = view.get_render_json(cls=SmartJSONEncoder)
# ist UNGLEICH schneller
render_json = view.get_render_json(ensure_ascii=False, cls=SmartJSONEncoder,
                                   indent=2, sort_keys=True)


# Dafür sorgen, dass bei unauthorisiertem Zugriff der Browser nach Passwort
# fragt (durch konvertieren von HTTP-Status Forbidden nach HTTP-Status
# Unauthorized)
@app.view(model=HTTPForbidden)
def http_forbidden(self, request):
    return view.to_unauthorized(self, request)


### AppRoot ###################################################################

@app.view(AppRoot, render_json, ReadPermission)
def app_root(self, request):
    links = {
        'students': request.link(model.StudentCollection()),
        'student_cards': request.link(model.StudentCardCollection()),
        'faculties': request.link(model.FacultyCollection()),
        'employees': request.link(model.EmployeeCollection()),
    }
    return view.dictify(self, links=links)


### Student* ##################################################################

@app.view(model.Student, render_json, ReadPermission)
def student(self, request):
    links = {
        'self': request.link(self),
        'collection': request.link(model.StudentCollection()),
    }
    return view.dictify(self, links=links)


@app.view(model.StudentCollection, render_json, ReadPermission)
def student_collection(self, request):
    links = {
        'self': request.link(self),
        'for_ldap': request.link(self, '+for_ldap'),
    }
    items = self.query(last_name = request.GET.get('last_name'))
    return view.dictify_collection(self, links=links,
                                   items=items, linkmaker=request.link)


@app.view(model.StudentCollection, render_json, ReadPermission, name='for_ldap')
def student_collection_for_ldap(self, request):
    links = {
        'self': request.link(self, '+for_ldap'),
        'main': request.link(self),
    }
    return view.dictify_collection(self, links=links, items=self.for_ldap(),
                                   linkmaker=request.link)


### StudentCard* ##############################################################

@app.view(model.StudentCard, render_json, ReadPermission)
def student_card(self, request):
    links = {
        'self': request.link(self),
        'collection': request.link(model.StudentCardCollection()),
    }
    return view.dictify(self, links=links)


@app.view(model.StudentCardCollection, render_json, ReadPermission)
def student_card_collection(self, request):
    links = {
        'self': request.link(self),
    }
    items = self.items()
    print(len(items))
    return view.dictify_collection(self, links=links, items=items,
                                   linkmaker=request.link)


### Faculty* ##################################################################

@app.view(model.Faculty, render_json, ReadPermission)
def faculty(self, request):
    links = {
        'self': request.link(self),
        'students': request.link(self, '+students'),
        'collection': request.link(model.FacultyCollection()),
    }
    return view.dictify(self, links=links)


@app.view(model.Faculty, render_json, ReadPermission, name='students')
def faculty_students(self, request):
    links = {
        'self': request.link(self, '+students'),
        'main': request.link(self),
    }
    return view.dictify_collection(self, links=links, items=self.students(),
                                   linkmaker=request.link)


@app.view(model.FacultyCollection, render_json, ReadPermission)
def faculty_collection(self, request):
    links = {
        'self': request.link(self),
    }
    return view.dictify_collection(self, links=links,
                                   items=self.items(), linkmaker=request.link)


### Employee* #################################################################

@app.view(model.Employee, render_json, ReadPermission)
def employee(self, request):
    links = {
        'self': request.link(self),
        'collection': request.link(model.EmployeeCollection()),
    }
    return view.dictify(self, links=links)


@app.view(model.EmployeeCollection, render_json, ReadPermission)
def employee_collection(self, request):
    links = {
        'self': request.link(self),
    }
    return view.dictify_collection(self, links=links,
                                   items=self.items(), linkmaker=request.link)
