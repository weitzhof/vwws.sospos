# stdlib
import logging

# projekt
from vwws.common.model import AppRoot
import vwws.sospos
from vwws.sospos import app
from vwws.sospos import model


log = logging.getLogger(__name__)


### AppRoot ###################################################################

@app.path(model=AppRoot, path='/')
def get_service_information():
    return AppRoot.from_package(vwws.sospos)


### Student* ##################################################################

@app.path(model=model.StudentCollection, path='/students')
def get_student_collection():
    return model.StudentCollection()


@app.path(model=model.Student, path='/students/{matno}')
def get_student(matno=0):
    return model.Student.get(matno)


### StudentCard* ##############################################################

@app.path(model=model.StudentCardCollection, path='/student_cards')
def get_student_card_collection():
    return model.StudentCardCollection()


@app.path(model=model.StudentCard, path='/student_cards/{cardno}')
def get_student_card(cardno):  # kein type hing: cardno ist vom typ str!
    return model.StudentCard.get(cardno)


### Faculty* ##################################################################

@app.path(model=model.FacultyCollection, path='/faculties')
def get_faculty_collection():
    return model.FacultyCollection()


@app.path(model=model.Faculty, path='/faculties/{id}')
def get_faculty(id):
    return model.Faculty.get(id)


### Employee* #################################################################

@app.path(model=model.EmployeeCollection, path='/employees')
def get_employee_collection():
    return model.EmployeeCollection()


@app.path(model=model.Employee, path='/employees/{empno}')
def get_employee(empno=0):
    return model.Employee.get(empno)
