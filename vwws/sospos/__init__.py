'''Verwaltungs-Webservices HAW Landshut: RESTful Interface für SOSPOS-DBs'''

# stdlib
import logging
import threading  # nur für threading.local

# 3rd party
import morepath
import sqlalchemy

# projekt
from vwws.common import auth, db
from vwws.common.env import Env


# Versionsstring muss PEP-440-konform sein
# vgl. https://python.org/dev/peps/pep-0440
__version__ = '0.1.dev0'

# Package-Level-Logger erzeugen -- gibt selbst nichts aus sondern propagiert
# Meldungen an den nächsthöheren Logger (sinnvoll für Service-Aggregation)
log = logging.getLogger(__name__)

# Thread-lokalen Namensraum für Paketweit genutzte Objekte erzeugen
# (morepath.settings macht einnen  zu umständlichen Eindruck)
mnt = threading.local()

# morepath-app definieren
class app(morepath.App):
    pass


def setup():
    '''Alles so weit konfigurieren, dass app gestartet werden kann'''

    # Logger Konfigurieren
    log.setLevel(str(Env('VWWS_SOSPOS_LOGLEVEL')))

    # Datenbankengine einrichten und registrieren
    url = str(Env('VWWS_SOSPOS_DB_URL'))
    pwd = str(Env('VWWS_SOSPOS_DB_PASSWORD'))
    engine = sqlalchemy.create_engine(db.bless_url(url, password=pwd))
    db.router.register_engine(db.tested_engine(engine))

    # Benutzer und Rechte ermitteln
    user_file = str(Env('VWWS_SOSPOS_USER_FILE'))
    group_file = str(Env('VWWS_SOSPOS_GROUP_FILE'))
    mnt.users = auth.get_users(user_file, group_file)

    # morepath konfigurieren
    # morepath.autosetup funktioniert nicht, wenn das Paket später per pip
    # produktiv (nicht "editable") installiert wird, daher explizite config
    # TODO: testen, ob dies immer noch so ist
    config = morepath.setup()
    config.scan()
    config.commit()


def run_devserver():
    '''Entwicklungsserver starten'''

    # Root-Logger konfigurieren (Devserver muss Logmeldungen nicht propagieren)
    logging.basicConfig(
        format='pid{process} {asctime} {levelname} {name}: {message}',
        datefmt='%Y-%m-%d %H:%M:%S',
        style='{'
    )

    logging.info('{} einrichten'.format(__name__))
    setup()

    logging.info('Entwicklungsserver starten')
    morepath.run(app())
