# stdlib
import logging

# 3rd party
from morepath import security

# projekt
from vwws.common import auth
from vwws.sospos import app, mnt


log = logging.getLogger(__name__)


class ReadPermission:
    name = 'read_permission'


@app.identity_policy()
def identity_policy():
    return security.BasicAuthIdentityPolicy()


@app.verify_identity()
def verify_identity(identity):
    return auth.is_valid_identity(identity, mnt.users)


@app.permission_rule(model=object, permission=ReadPermission)
def read_permission(identity, model, permission):
    user = mnt.users[identity.userid]
    return user.is_admin or user.in_group('read')
