'''vwws.sospos: WSGI-Applikation'''

# projekt
import vwws.sospos


def wsgi_factory():
    '''Fertig konfigurierte WSGI-Applikation zurückliefern'''
    vwws.sospos.setup()
    return vwws.sospos.app()


application = wsgi_factory()
