# stdlib
import logging
from textwrap import dedent

# 3rd party
from sqlalchemy.exc import DBAPIError

# projekt
from vwws.common.model import FlexModel, Collection, WithEngine
from vwws.common.util import time_this


log = logging.getLogger(__name__)


# Es wäre schön, wenn sich SQLAlchemy hier mehr ausnutzen ließe -- allerdings
# wären die dabei entstehenden Schemata dann vermutlich zu groß (165 Spalten
# allein für die Tabelle 'sos') und voller Spezialfälle (CHAR-Felder anstatt
# VARCHAR benötigen trim; Spalten müssen der Konsistenz halber umbenannt werden
# usw.). Daher wird hier vorerst mit ganz normalem SQL gearbeitet)


### Student* ##################################################################


class Student(FlexModel, WithEngine):

    _fields = [
        'matno',
        'last_name',
        'first_name',
        'gender',
        'date_of_birth',
        'immatriculation_date',
        'exmatriculation_date',
        'bibno',
    ]
    _required_fields = ['matno']

    @classmethod
    def get(cls, matno):
        with cls.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    mtknr AS matno,
                    trim(nachname) AS last_name,
                    trim(vorname) AS first_name,
                    CASE geschl WHEN 'M' THEN 'm'
                                WHEN 'W' THEN 'f'
                                ELSE 'u' END AS gender,
                    gebdat AS date_of_birth,
                    immdat AS immatriculation_date,
                    exmdat AS exmatriculation_date,
                    trim(bibnr) AS bibno
                FROM
                    sos
                WHERE
                    mtknr = %(matno)s''', matno=matno
            )
            row = result.fetchone()
            return cls(**row) if row else None


class StudentCollection(Collection, WithEngine):

    def query(self, last_name=None):
        if not last_name:
            return []
        with self.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    mtknr AS matno,
                    trim(nachname) AS last_name,
                    trim(vorname) AS first_name
                FROM
                    sos
                WHERE
                    exmdat IS NOT NULL AND
                    trim(nachname) = %(last_name)s
                ORDER BY
                    last_name, first_name''', last_name=last_name
            )
            return [Student(**row) for row in result.fetchall()]

    def for_ldap(self):
        # DISTINCT ON (mtknr): Matrikelnummer darf jeweils nur 1x vorkommen.
        #
        # Da die Studiengänge in ORDER BY nach Relevanz sortiert sind (Der
        # zuletzt angefangene zuerst, bei Mehrdeutigket der höchste Abschluss
        # zuerst, bei Mehrdeutigkeit alphabetisch), wird jeweils der
        # relevanteste Studiengang/Fachbereich zu einer Matrikelnummer
        # geliefert.
        with self.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT DISTINCT ON (mtknr)
                    sos.mtknr AS matno,
                    trim(sos.nachname) AS last_name,
                    trim(sos.vorname) AS first_name,
                    CASE sos.geschl WHEN 'M' THEN 'm'
                                    WHEN 'W' THEN 'f'
                                    ELSE 'u' END AS gender,
                    sos.gebdat AS date_of_birth,
                    sos.immdat AS immatriculation_date,
                    sos.exmdat AS exmatriculation_date,
                    stg.abschl AS degree_id,
                    stg.stg as course_of_study_id,
                    stg.fb AS faculty,
                    stg.lepsem AS semester,
                    trim(k_stg_bund.ltxt) AS course_of_study,
                    trim(sos.bibnr) AS bibno,
                    trim(k_fb.ktxt) AS faculty_id
                FROM
                    sos
                    JOIN stg USING (mtknr, semester)
                    LEFT JOIN k_stg_bund ON k_stg_bund.stg = stg.stg
                    LEFT JOIN k_fb  ON k_fb.fb  = stg.fb
                WHERE
                    sos.exmdat IS NULL OR current_date - 365 <= sos.exmdat
                ORDER BY
                    mtknr,
                    stg.anfdat DESC,
                    stg.abschl DESC,
                    k_stg_bund.ltxt'''
            )
            return [Student(**row) for row in result.fetchall()]


### StudentCard* ##############################################################

# diese Tabellen existieren nur an der Hochschule Landshut
class StudentCard(FlexModel, WithEngine):

    _fields = [
        'cardno',
        'matno',
        'bibno',
        'valid_until',
        'disabled',
        'created',
        'changed',
    ]
    _required_fields = ['cardno']

    @classmethod
    def get(cls, cardno):
        with cls.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    cardnr AS cardno,
                    mtknr AS matno,
                    bibnr AS bibno,
                    gueltig_bis AS valid_until,
                    ist_gesperrt AS disabled,
                    registriert AS created,
                    geaendert AS changed
                FROM
                    studausw
                WHERE
                    cardnr = %(cardno)s''', cardno=cardno
            )
            row = result.fetchone()
            return cls(**row) if row else None


class StudentCardCollection(Collection, WithEngine):

    def items(self):
        with self.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    cardnr AS cardno,
                    mtknr AS matno,
                    bibnr AS bibno,
                    gueltig_bis AS valid_until,
                    ist_gesperrt AS disabled,
                    registriert AS created,
                    geaendert AS changed
                FROM
                    studausw
                ORDER BY
                    registriert'''
            )
            return [StudentCard(**row) for row in result.fetchall()]


### Faculty* ##################################################################

class Faculty(FlexModel, WithEngine):

    _fields = ['id', 'name']
    _required_fields = ['id']

    @classmethod
    def get(cls, id):
        with cls.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    trim(ktxt) AS id,
                    trim(ltxt) AS name
                FROM
                    k_fb
                WHERE
                    trim(ktxt) = %(id)s
                ORDER BY
                    id''', id=id
            )
            row = result.fetchone()
            return cls(**row) if row else None

    def students(self):
        with self.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    sos.mtknr            AS matno
                    , trim(sos.vorname)  AS first_name
                    , trim(sos.nachname) AS last_name
                    , trim(email.email)  AS email
                FROM
                    sos
                    JOIN stg        USING (mtknr, semester)
                    JOIN k_stg      USING (stg)
                    JOIN k_fb       ON k_fb.fb = stg.fb
                    JOIN identroll  ON identroll.verbindung_integer = mtknr
                    LEFT JOIN email ON email.identnr = identroll.identnr
                WHERE
                    TRUE
                    AND k_fb.ktxt = %(id)s
                    AND identroll.rolle = 'S'
                    AND email.artkz = 'RZ'
                    AND stg.anfdat <= current_date
                    AND (current_date <= stg.endedat OR stg.endedat IS NULL)
                ORDER BY
                    last_name, first_name''', id=self.id
            )
            return [Student(**row) for row in result.fetchall()]


class FacultyCollection(Collection, WithEngine):

    def items(self):
        with self.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT DISTINCT
                    trim(ktxt) AS id,
                    trim(ltxt) AS name
                FROM
                    k_fb
                ORDER BY id'''
            )
            return [Faculty(**row) for row in result.fetchall()]


### Employee* #################################################################

class Employee(FlexModel, WithEngine):

    _fields = [
        'empno',
        'first_name',
        'last_name',
        'bibno',
        'gender',
    ]
    _required_fields = ['empno']

    @classmethod
    def get(cls, empno):
        with cls.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    pnr AS empno,
                    trim(vorname) AS first_name,
                    trim(nachname) AS last_name,
                    bibnr AS bibno,
                    CASE geschl WHEN 'M' THEN 'm'
                                WHEN 'W' THEN 'f'
                                ELSE 'u' END AS gender
                FROM
                    verwaltung
                WHERE
                    pnr = %(empno)s''', empno=empno
            )
            row = result.fetchone()
            return cls(**row) if row else None


class EmployeeCollection(Collection, WithEngine):

    def items(self):
        with self.get_engine().begin() as conn:
            result = conn.execute(
                '''\
                SELECT
                    pnr AS empno,
                    trim(vorname) AS first_name,
                    trim(nachname) AS last_name,
                    bibnr AS bibno
                FROM
                    verwaltung
                ORDER BY
                    nachname, vorname'''
            )
            return [Employee(**row) for row in result.fetchall()]
