===========
vwws.sospos
===========

Verwaltungs-Webservices HAW Landshut: RESTful Interface für SOSPOS-Datenbanken

(Momentan Read-Only)


Installation
============

Vorausgesetzt werden ein aktuelles Betriebssystem (vorzugsweise Linux) sowie
folgende Software:


- Python_ 3.3 oder neuer

- Eine Umgebung zum Kompilieren von in C geschriebenen Python-3-Bibliotheken

- Git_ zum Herunterladen (Klonen) von Git-Repositories

- virtualenv_ zum Erstellen isolierter Python-Umgebungen

- pip_ zum Installieren von Python-Paketen


Unter Debian_ (ab Version 8) bzw. Ubuntu_ (Ab Version 13.04) reichen dafür
folgende Kommandos::

 $ sudo apt-get install python3 python3-dev build-essential
 $ sudo apt-get install git python-virtualenv python-pip


virtualenv einrichten
---------------------

Falls noch keine virtuelle Python-Umgebung (*virtualenv*) für
Verwaltungs-Webservices besteht, kann diese folgendermaßen angelegt werden::

 $ virtualenv -p /usr/bin/python3 /pfad/zu/vwws-env

virtualenv aktivieren::

 $ source /pfad/zu/vwws-env/bin/activate

Im Folgenden wird davon ausgegangen, dass immer mit aktiviertem virtualenv
gearbeitet wird.


vwws.sospos installieren
------------------------

vwws.common_ (Dependency) und vwws.sospos installieren::

 $ pip install git+https://bitbucket.org/weitzhof/vwws.common
 $ pip install git+https://bitbucket.org/weitzhof/vwws.sospos

**Hinweis:** Alle Komponenten von vwws können im selben virtualenv installiert
werden.  Falls vwws.common dort bereits vorliegt, muss dieses Paket natürlich
nicht neu installiert werden.


Erster Start (Entwicklungsmodus)
================================

Vorausgesetzt wird eine in PostgreSQL_ eingerichtete HIS-GX_-SOSPOS-Datenbank.
Entwickelt wurde gegen die Version 16 von HIS-GX, es ist jedoch denkbar, dass
vwws.sospos auch mit anderen Versionen funktioniert.

vwws.sospos wird über Umgebungsvariablen konfiguriert (vgl. 12factor_).
Anzugeben sind dabei unter anderem Datenbankeinstellungen und Pfade zu
Benutzer- und Gruppendateien.

Ein interaktives Skript, das diese Umgebungsvariablen setzt, sowie
Beispiel-Benutzer- und Gruppendateien finden sich im Unterverzeichnis
``extras/develop/`` des Quellcodes. Dieser kann wie folgt heruntergeladen
(geklont) werden::

 $ git clone https://bitbucket.org/weitzhof/vwws.sospos

Unter Bash kann das System dann für einen Start im Entwicklungsmodus
folgendermaßen eingerichtet werden::

 $ source /pfad/zu/vwws.sospos/extras/develop/env-setup.develop

Danach kann mit folgendem Kommando ein Entwicklungsserver gestartet werden::

 $ vwws.sospos-devserver

Dieser Server ist dann unter ``http://localhost:5000`` z.B. per Browser
erreichbar. Mit Benutzer ``devadmin``, Passwort ``pass`` per HTTP-Basic-Auth
authentifiziert, sollten die auf der Startseite sichtbaren Informationen
ausreichen, um sich einen Überblick über die Funktionalität des Services
verschaffen zu können (vgl. HATEOAS_).


Zugriff auf vwws.sospos
=======================

vwws.sospos versucht, ein RESTful (vgl. REST_) Interface zu einer
SOSPOS-Datenbank über HTTP zur Verfügung zu stellen. Daten werden dabei im
JSON_-Format kodiert.

Neben dem Zugriff per Browser ist eine (von unzähligen weiteren) Möglichkeiten
dieses Interface anzusprechen die Python-Bibliothek Requests_ (Kompatibel zu
Python 2 und Python 3).

Der Zugriff auf den Service mit Requests ist trivial (hier ohne Beschränkung
der Allgemeinheit für vwws.sospos auf localhost:5000; das Ergebnis ist zur
besseren Lesbarkeit eingerückt)::

 $ python3
 >>> import requests
 >>> auth_data = ('devadmin', 'pass')
 >>> response = requests.get('http://localhost:5000/faculties, auth=auth_data)
 >>> response.json()
 {
     '__type__': 'FacultyCollection',
     '__links__': {'self': '/faculties'},
     '__items__': [
        {
            '__type__': 'Faculty',
            '__links__': {'item': '/faculties/IF'},
            'id': 'IF',
            'name': 'Informatik'
        },
        {
            '__type__': 'Faculty',
            '__links__': {'item': '/faculties/MB'},
            'id': 'MB',
            'name': 'Maschinenbau'
        }
     ]
 }


Produktivbetrieb
================

TODO


.. _12factor: http://12factor.net
.. _Debian: https://www.debian.org
.. _Git: http://git-scm.com
.. _HATEOAS: https://en.wikipedia.org/wiki/HATEOAS
.. _HIS: http://www.his.de
.. _HIS-GX: http://www.his.de/abt1/gx
.. _JSON: https://de.wikipedia.org/wiki/JSON
.. _morepath: https://github.com/morepath/morepath
.. _pip: https://pip.pypa.io
.. _PostgreSQL: http://www.postgresql.org
.. _Python: https://www.python.org
.. _Requests: http://python-requests.org
.. _REST: https://en.wikipedia.org/wiki/REST
.. _Ubuntu: http://www.ubuntu.com
.. _virtualenv: https://virtualenv.readthedocs.org
.. _vwws.common: https://bitbucket.org/weitzhof/vwws.common
.. _WSGI: http://de.wikipedia.org/wiki/Web_Server_Gateway_Interface
