#!/usr/bin/env python3

'''Setupskript für vwws.sospos'''

# stdlib
import os
import re
import sys

# 3rd party
from setuptools import setup, find_packages


# Python 3.3 oder neuer voraussetzen
assert sys.version_info >= (3, 3), "Python 3.3 oder neuer benötigt"


# Warum Quelltextanalyse anstatt import? vgl.
# https://groups.google.com/d/topic/pypa-dev/0PkjVpcxTzQ/discussion
def find_version(path):
    '''
    Versionsstring in Quelltextdatei suchen und zurückliefern

    Die Zeile, die den Versionsstring enthält muss folgendermaßen aussehen:

    __version__ = 'VERSION'

    wobei 'VERSION' ein PEP-440-kompatibler Versionsidentifikator sein muss

    '''
    with open(path, encoding='utf-8') as f:
        contents = f.read()
    match = re.search(r'^__version__ = [\'"]([^\'"]*)[\'"]',
                      contents, re.MULTILINE)
    if match:
        return match.group(1)
    raise RuntimeError('Kann Versionsstring nicht ermitteln')


# In Verzeichnis des Setup-Skripts wechseln
os.chdir(os.path.dirname(__file__))

# Lange Beschreibung aus README lesen
with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='vwws.sospos',
    version=find_version(os.path.join('vwws', 'sospos', '__init__.py')),
    description='Verwaltungs-Webservices HAW Landshut: '
                'RESTful Interface für SOSPOS-Datenbanken',
    long_description=long_description,
    keywords='rest webservice haw hochschule landshut verwaltung sospos',
    url='https://bitbucket.org/weitzhof/vwws.sospos',
    author='Bernhard Weitzhofer',
    author_email='weitzhof@haw-landshut.de',
    license='GPLv3+',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment'
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'Intended Audience :: System Administrators',
        'License :: DFSG approved',
        'License :: OSI Approved '
                ':: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: German',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    namespace_packages=['vwws'],
    install_requires=[
        'morepath>=0.4.1',
        'py-postgresql>=1.1',
        'sqlalchemy>=0.9',
        'vwws.common>=0.1.dev0',
    ],
    entry_points={
        'console_scripts': [
            'vwws.sospos-devserver = vwws.sospos:run_devserver',
        ]
    }
)
